class FSM:
    def __init__(self, alphabet, states, start_state, finite_states, transitions):
        self.alphabet = alphabet
        self.states = states
        self.current_state = start_state
        self.finite_states = finite_states
        self.transitions = transitions

    def __check_exists_transitions(self, state, symbol):
        return self.transitions[state] and self.transitions[state][symbol]

    def __change_state(self, symbol):
        if self.__check_exists_transitions(self.current_state, symbol):
            self.current_state = self.transitions[self.current_state][symbol]
        else:
            raise Exception(f'No transitions from {self.current_state} by {symbol}')

    def __check_belong_alphabet(self, symbol):
        if symbol not in self.alphabet:
            raise Exception(f'Unknown symbol {symbol}')

    def test(self, value):
        for s in value:
            self.__check_belong_alphabet(s)
            self.__change_state(s)

        return self.current_state in self.finite_states
