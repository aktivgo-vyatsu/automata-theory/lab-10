import string

from fsm import FSM


def create_transitions_from_state_and_symbols(symbols, target_state):
    result = {}

    for s in symbols:
        result[s] = target_state

    return result


if __name__ == '__main__':
    v1 = list(string.ascii_lowercase)
    v2 = list('!@#$%^&*()')
    v3 = list(string.digits)

    v = v1 + v2 + v3

    q = ['q0', 'q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'q7', 'q8', 'q9', 'q10']

    s = 'q0'

    f = ['q10']

    transitions = {
        'q0': create_transitions_from_state_and_symbols(v1, 'q1') | create_transitions_from_state_and_symbols(v2, 'q4') | create_transitions_from_state_and_symbols(v3, 'q7'),
        'q1': create_transitions_from_state_and_symbols(v1, 'q1') | create_transitions_from_state_and_symbols(v2, 'q2') | create_transitions_from_state_and_symbols(v3, 'q3'),
        'q2': create_transitions_from_state_and_symbols(v1, 'q2') | create_transitions_from_state_and_symbols(v2, 'q2') | create_transitions_from_state_and_symbols(v3, 'q10'),
        'q3': create_transitions_from_state_and_symbols(v1, 'q3') | create_transitions_from_state_and_symbols(v2, 'q10') | create_transitions_from_state_and_symbols(v3, 'q3'),
        'q4': create_transitions_from_state_and_symbols(v1, 'q5') | create_transitions_from_state_and_symbols(v2, 'q4') | create_transitions_from_state_and_symbols(v3, 'q6'),
        'q5': create_transitions_from_state_and_symbols(v1, 'q5') | create_transitions_from_state_and_symbols(v2, 'q5') | create_transitions_from_state_and_symbols(v3, 'q10'),
        'q6': create_transitions_from_state_and_symbols(v1, 'q10') | create_transitions_from_state_and_symbols(v2, 'q6') | create_transitions_from_state_and_symbols(v3, 'q6'),
        'q7': create_transitions_from_state_and_symbols(v1, 'q8') | create_transitions_from_state_and_symbols(v2, 'q9') | create_transitions_from_state_and_symbols(v3, 'q7'),
        'q8': create_transitions_from_state_and_symbols(v1, 'q8') | create_transitions_from_state_and_symbols(v2, 'q10') | create_transitions_from_state_and_symbols(v3, 'q8'),
        'q9': create_transitions_from_state_and_symbols(v1, 'q10') | create_transitions_from_state_and_symbols(v2, 'q9') | create_transitions_from_state_and_symbols(v3, 'q9'),
        'q10': create_transitions_from_state_and_symbols(v1, 'q10') | create_transitions_from_state_and_symbols(v2, 'q10') | create_transitions_from_state_and_symbols(v3, 'q10'),
    }

    fsm = FSM(v, q, s, f, transitions)

    print(fsm.test('1$1212121'))
